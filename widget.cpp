#include "widget.h"
#include <vlc/vlc.h>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

    vlc = new QWidget(this);
    vlc->setGeometry(0,0,800, 600);

    overlay = new QWidget(this);
    overlay->setStyleSheet("background-color:rgba(100,100,100,0.5)");
    overlay->setGeometry(0,0,400, 600);

    libvlc_instance_t * inst;
    libvlc_media_player_t *mp;
    libvlc_media_t *m;

    /* Load the VLC engine */
    inst = libvlc_new (0, NULL);

    /* Create a new item */
    m = libvlc_media_new_location (inst, "https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");

    /* Create a media player playing environement */
    mp = libvlc_media_player_new_from_media (m);
    libvlc_media_player_set_xwindow(mp, vlc->winId());

    /* No need to keep the media now */
    libvlc_media_release (m);

    /* play the media_player */
    libvlc_media_player_play (mp);

    overlay->raise();


    /* Stop playing */
    //    libvlc_media_player_stop (mp);

    /* Free the media_player */
    //    libvlc_media_player_release (mp);

    //    libvlc_release (inst);

}

Widget::~Widget()
{

}
