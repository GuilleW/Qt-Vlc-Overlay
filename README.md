# Qt-Vlc-Overlay

Simple and small test with Qt + libVLC + QWidget overlay.

## Getting Started

<span style="color:red">This project doesn't work actually.</span>
I'll update this project as soon as I found a solution.

### Prerequisites

Of course, Qt. And install VLC lib.

```
apt install libvlc-dev
```

## Running the tests

Download project and open Qt-Vlc-Overlay.pro in Qt.
The overlay should be semi-transparent with an 50% opacity.
If you know how to fix it, feel free to patch it or contact me.
Thank you.
